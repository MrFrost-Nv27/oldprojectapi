<?php


require "../config/connect.php";


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $response = array();
    $token = $_POST['token'] ?? '';

    $cek = mysqli_query($con, "SELECT * FROM api_settings WHERE token='$token'");
    if (mysqli_num_rows($cek) > 0) {

        $response['penduduk'] = array();

        $cekPenduduk = mysqli_query($con, "SELECT * FROM sidb_penduduk");
        while ($a  = mysqli_fetch_array($cekPenduduk)) {
            $response['penduduk'][] = array(
                "id" => (int)$a['id'],
                "nama" => $a['nama'],
                "no_kk" => (int)$a['no_kk'],
                "nik" => (int)$a['nik'],
                "jenis_kelamin" => $a['jenis_kelamin'],
                "tempat_lahir" => $a['tempat_lahir'],
                "tanggal_lahir" => $a['tanggal_lahir'],
                "agama" => $a['agama'],
                "pendidikan" => $a['pendidikan'],
                "hubungan_keluarga" => $a['hubungan_keluarga'],
                "created_at" => $a['created_at'],
                "updated_at" => $a['updated_at'],
                "deleted_at" => $a['deleted_at'],
            );
        }
        $response['value'] = 1;
        $response['message'] = "Success";
        echo json_encode($response);
    } else {
        # code...
        $response['value'] = 0;
        $response['message'] = "Token Invalid";
        echo json_encode($response);
    }
}
<?php

// 3290uu24u204jdsvbdvbfgr

require "../config/connect.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $response = array();
    $token = $_POST['token'] ?? '';

    $cek = mysqli_query($con, "SELECT * FROM api_settings WHERE token='$token'");
    if (mysqli_num_rows($cek) > 0) {

        $response['hotel'] = array();
        $response['wisata'] = array();
        $response['makanan'] = array();

        $cekHotel = mysqli_query($con, "SELECT * FROM guci_hotel");
        $cekWisata = mysqli_query($con, "SELECT * FROM guci_wisata");
        $cekMakanan = mysqli_query($con, "SELECT * FROM guci_makanan");
        while ($a  = mysqli_fetch_array($cekHotel)) {
            $gambar = $a['gambar'];
            $size = getimagesize("$url/files/image/guci/hotel/$gambar");
            $response['hotel'][] = array(
                "id" => (int)$a['id'],
                "nama" => $a['nama'],
                "harga" => (int)$a['harga'],
                "gambar" => $gambar,
                "created_at" => $a['created_at'],
                "updated_at" => $a['updated_at'],
                "deleted_at" => $a['deleted_at'],
            );
        }
        while ($a  = mysqli_fetch_array($cekWisata)) {
            $gambar = $a['gambar'];
            $size = getimagesize("$url/files/image/guci/wisata/$gambar");
            $response['wisata'][] = array(
                "id" => (int)$a['id'],
                "nama" => $a['nama'],
                "harga" => (int)$a['harga'],
                "gambar" => $gambar,
                "created_at" => $a['created_at'],
                "updated_at" => $a['updated_at'],
                "deleted_at" => $a['deleted_at'],
            );
        }
        while ($a  = mysqli_fetch_array($cekMakanan)) {
            $gambar = $a['gambar'];
            $size = getimagesize("$url/files/image/guci/makanan/$gambar");
            $response['makanan'][] = array(
                "id" => (int)$a['id'],
                "nama" => $a['nama'],
                "harga" => (int)$a['harga'],
                "gambar" => $gambar,
                "created_at" => $a['created_at'],
                "updated_at" => $a['updated_at'],
                "deleted_at" => $a['deleted_at'],
            );
        }
        $response['value'] = 1;
        $response['message'] = "Success";
        echo json_encode($response);
    } else {
        # code...
        $response['value'] = 0;
        $response['message'] = "Token Invalid";
        echo json_encode($response);
    }
}